


def main()
	parser=argparse.ArgumentParser(formatter_class=\
		argparse.ArgumentDefaultsHelpFormatter)
		
	parse
	parser.add_argument('--filename_datos',default='tp1_ej2_training.csv',
										help='el procesamiento de los datos en el script fue realizado utilizando un .csv con los targets en la primera columna', 
										required=True)
	
	parser.add_argument('--filename_modelo',default='nnmodel.json',help='nombre del archivo que es exportado y contiene el modelo entrenado')
	
	parser.add_argument('--S', help='Nodos por capa sin contar entrada ni salida, separados por coma, sin espacios ni []',default='5')
	parser.add_argument('--lr', help='learning rate',type=float,default=0.01)
	parser.add_argument('--activation', help='tanh o sigmoid',default='sigmoid')
	parser.add_argument('--alfa_momento', help='entre 0 y 1',default=0,type=float)
	parser.add_argument('--epocas', default=8000,type=int)
	parser.add_argument('--exportar',default=True,help='si el usuario desea \
					exportar el modelo entrenado al archivo filename_modelo.npz')
	# parser.add_argument('--B',help='batch size',default='P')
	args=parser.parse_args()
	
	
if __name__ == "__main__":
    main()
