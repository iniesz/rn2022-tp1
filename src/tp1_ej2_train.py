import argparse
import pandas
import mnn
import numpy as np


def separate_validation_from_training(all_inputs_X, all_targets_Y, validation_size):
  assert(all_inputs_X.shape[0] > validation_size)
  assert(all_inputs_X.shape[0] == all_targets_Y.shape[0])
  training_size = all_inputs_X.shape[0] - validation_size
  validations_size = all_inputs_X.shape[0] - training_size
  
  training_inputs_X = all_inputs_X[0:training_size:]
  validation_inputs_X = all_inputs_X[-validations_size:]

  training_targets_Y = all_targets_Y[0:training_size:]
  validation_targets_Y = all_targets_Y[-validations_size:]

  return (training_inputs_X, training_targets_Y), (validation_inputs_X, validation_targets_Y)

def parse_args(): 
	parser=argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
	parser.add_argument('--filename_datos',default='tp1_ej2_training.csv',
										help='el procesamiento de los datos en el script fue realizado utilizando un .csv con los targets en la primera columna', 
										required=True)
	
	parser.add_argument('--filename_modelo',default='nnmodel.json',
						help='nombre del archivo que es exportado y contiene el modelo entrenado', required=True)
	
	parser.add_argument('--hidden_layers', help='Cantidad de unidades por capa oculta. Numeros separados por coma, ej:2,3,5.', default='4,4')
	parser.add_argument('--activation_functions', help='Funciones de activación por capa. Opciones relu, identity, tanh o sigmoid', default='sigmoid,sigmoid,sigmoid')
	parser.add_argument('--lr', help='learning rate',type=float, default=0.1)
	parser.add_argument('--epocs', default=400,type=int)
	
	parser.add_argument('--error_tolerance', help='Tolerancia de error para calcular el accuracy. El valor real esta escalado MinMax por normalizacion MinMax\
												y depende del training set y el feature. ',type=float, default=0.1)
	
	parser.add_argument('--accuracy_target', help='Condición de corte de accuracy. Depende del error_tolerance y del set de validación.', default=0.8,type=float)
	
	parser.add_argument('--validation_size', help='Tamaño del conjunto de validación.', default=20,type=int)
	parser.add_argument('--batch_size', help='Tamaño de batch.', default=50,type=int)

  parser.add_argument('--random_seed', default=1984, type=int)
	
	#parser.add_argument('--exportar',default=True,help='si el usuario desea exportar el modelo entrenado al archivo filename_modelo.npz')
	# parser.add_argument('--B',help='batch size',default='P')
	args=parser.parse_args()

def main():
  args = parse_args()
  df = pandas.read_csv('/content/rn2022-tp1-datasets/datasets/tp1_ej2_training.csv', header=None)

  ## Permutation of the dataset
  dataset = np.random.permutation(df.to_numpy())
  pandas.DataFrame(dataset)

  ## Nos quedamos con una permutacion de las entradas y los targets normalizados
  all_inputs_X = np.delete(dataset, [8,9], axis=1)
  all_targets_Y = dataset[:,[8,9]]

  ##
  inputs_normalizer = mnn.MinMaxNormalizer(all_inputs_X)
  outputs_normalizer = mnn.MinMaxNormalizer(all_targets_Y)

  all_inputs_X = inputs_normalizer.normalization(all_inputs_X)
  all_targets_Y = outputs_normalizer.normalization(all_targets_Y)

  training, validation = separate_validation_from_training(all_inputs_X, all_targets_Y, validation_size=50)
  (training_inputs_X, training_targets_Y) = training
  (validation_inputs_X, validation_targets_Y)  = validation


  
	
if __name__ == "__main__":
    main()
