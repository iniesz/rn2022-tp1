import numpy as np
import json

def identity(x):
    return x

def derivate_identity(x):
    return 0.0

def sigmoid(x):
    s=1/(1+np.exp(-x))
    ds=s*(1-s)  
    return s

def derivate_sigmoid(x):
    s = sigmoid(x)
    ds=s*(1-s)  
    return ds

def tanh(x):
    t=(np.exp(x)-np.exp(-x))/(np.exp(x)+np.exp(-x))
    return t


def derivate_tanh(x):
    t=tanh(x)
    dt=1-t**2
    return dt

def relu (x):
    return max(0.0, x)
  
def derivate_relu(x):
    return 1.0 * (x > 0.0)

activation_functions = {
  "identity" : {
        "function":identity,
        "derivate_function":derivate_identity      
    },
  "sigmoid" : {
      "function": sigmoid,
      "derivate_function" : derivate_sigmoid
  },
  "tanh" : {
      "function": tanh,
      "derivate_function" : derivate_tanh
  },
  "relu": {
      "function": relu,
      "derivate_function": derivate_relu
  }

}


import json
class LayerJsonEncoder(json.JSONEncoder):
    def default (self, obj):
      if  isinstance(obj, Layer):
          return {"Layer": 
                  {"inputs_size": obj._inputs_size, 
                  "outputs_size": obj._outputs_size, 
                  "learning_rate": obj._learning_rate,
                   "activation_function": obj._act_func_id,
                  "weights_W":  obj._weights_W.tolist()
                  }
                }
      return json.JSONEncoder.default(self, obj)


## Basado en el siguiente articulo 
## https://stackoverflow.com/questions/48991911/how-to-write-a-custom-json-decoder-for-a-complex-object
class LayerJsonDecoder(json.JSONDecoder):
    # "Hook" de nuestro decoder al decoder de json
    def __init__(self, *args, **kwargs):
      json.JSONDecoder.__init__(self, object_hook=self.object_hook, *args, **kwargs)

    def object_hook (self, dct):
      if "Layer" in dct:
        #print(repr(dct))
        inputs_size = dct["Layer"]["inputs_size"]
        outputs_size = dct["Layer"]["outputs_size"]
        learning_rate = dct["Layer"]["learning_rate"]
        act_func_id = dct["Layer"]["activation_function"]
        layer = Layer(inputs_size= inputs_size, 
                      outputs_size=outputs_size, 
                      learning_rate=learning_rate, 
                      act_func_id=act_func_id)
        weights_W = np.array(dct["Layer"]["weights_W"])
        layer._weights_W = weights_W
        return layer

      return dct


class Layer(object):
  # Constructor
  def __init__(self, inputs_size, outputs_size, learning_rate=0.1, act_func_id="identity"):
    self._inputs_size = inputs_size
    self._outputs_size = outputs_size

    # Initialize weights as a uniform normal distribution and insert bias
    self._weights_W = np.random.uniform(0.0, 1.0, size=(inputs_size+1, outputs_size))

    self._learning_rate = learning_rate

    self.set_activation_function(act_func_id)


  def __repr__(self):
        return json.dumps(self, cls=LayerJsonEncoder)

  def set_activation_function(self, act_func_id):
      self._act_func_id = act_func_id
      self._activation_function = np.vectorize(activation_functions[act_func_id]["function"])
      self._derivate_activation_function = np.vectorize(activation_functions[act_func_id]["derivate_function"])


  def predict(self, inputs_X):
    return self.activation_function(
                self.net_sum_inputs(inputs_X))

  #
  # return g(net_sum_inputs) con g la funcion de activacion
  #
  def activation_function(self, X):
    f = self._activation_function
    return f(X)


  def derivate_activation_function(self, X):
    df = self._derivate_activation_function
    return df(X)


  #
  # inputs_X es un array de dimensiones SxN donde S es el tamaño del batch y N la cantidad
  # de inputs del Layer.
  # Si el metodo recibe un array de una dimension este se toma como un array de dimensiones 1xN.
  # Esto permite transparencia para operacioness batch, minibatch y secuencial.
  #
  # net_sum_H es un array de dimensiones (SxN) . (NxM) = SxM
  #
  # outputs_Y es el array de dimensiones SxM resultado de la operacion feedforward en la capa actual.
  # 
  #    
  def feedforward(self, inputs_X):
    ## Transformamos vectores de dimension N en vectores de 1xN
    inputs_X = np.atleast_2d(inputs_X)

    net_sum_H = self.net_sum_inputs(inputs_X)
    outputs_Y = self.activation_function(net_sum_H)
    return net_sum_H, outputs_Y



  def correct_weights(self, delta_W):
    ## Assert por motivos de debug
    assert(self._weights_W.shape == delta_W.shape)
    self._weights_W += (self._learning_rate * delta_W)




  def backpropagation(self, inputs_X, net_sum_H, grad_outputs_Y):
    weights_W = self._weights_W

    #
    # S tamaño del batch
    # N cantidad de inputs del layer actual (es el M del layer anterior)
    # M cantidad de outputs del layer actual (es el N del layer siguiente)
    #
    # grad_outputs_Y -> dimensiones SxM
    # 
    # weights_W -> dimensiones (N+1)xM
    #   => weights_W.T -> dimensiones Mx(N+1)
    #   => delete_bias(weights_W.T) -> dimensiones MxN
    #
    # => grad_inputs_X -> dimensiones SxM · MxN = SxN
    # 
    # net_sum_H -> dimensiones = SxM
    # 
    grad_inputs_X = np.dot(grad_outputs_Y, self.delete_bias(weights_W.T))
    dY = self._derivate_activation_function(net_sum_H)

    #
    # Producto externo entre la derivada de la funcion de activacion aplicada a la suma pesada
    # sin activar y el gradiente de la salida.
    # Este delta es el delta de la capa actual del libro de haykins.
    # Las dimensioens son las mismas que las del gradiente de la salida.
    # (SxM^L) * (SxM^L) = SxM^L
    #
    delta = dY * grad_outputs_Y

    #
    # inputs_X es de dimensiones SxN con S tamaño del batch y N cantidad de inputs
    #   => inputs_X.T es de dimensiones NxS
    #   => insert_bias(inputs_X).T es de dimensiones (N+1) x S
    # grad_output_Delta es de dimensiones SxM
    #   => El dot product es de dimensiones (N+1xS) x (SxM) = (N+1xM)
    #  
    # (N+1) x M es efectivamente las dimensiones de la matriz de pesos del Layer
    #
    delta_W = np.dot(self.insert_bias(inputs_X).T, delta)

    self.correct_weights(delta_W)
    # ## Assert por motivos de debug
    # assert(weights_W.shape == delta_W.shape)

    # weights_W += (self._learning_rate * delta_W)
    # self._weights_W[:] = weights_W

    return grad_inputs_X

  #
  # inputs_X pertenece a SxN donde N es la cantidad de inputs y S el tamaño del batch
  # weights_W pertenece a (N+1)xM donde N es la cantidad de inputs y M la cantidad de outputs
  #   => outputs_Y de dimensiones (Sx(N+1)) x ((N+1) x M) = S x M 
  # Si se utiliza un batch de tamaño S la salida es una matriz SxM
  # Si se utiliza un solo ejemplo la salida es un vector 1xM
  #  
  def net_sum_inputs(self, inputs_X):
    weights_W = self._weights_W
    outputs_Y = np.dot(self.insert_bias(inputs_X), weights_W)
    return outputs_Y

  def insert_bias(self, v, bias = 1):
    bias_array = np.ones((v.shape[0],1)) * bias
    return np.hstack((bias_array, v))

  def delete_bias(self, v):

    ## Delete index 0 column
    return np.delete(v, 0, 1)
	
	
class NetworkJsonEncoder(json.JSONEncoder):
    def default (self, obj):
      if  isinstance(obj, Network):
          net_dct = {"Network":
                      {
                        "inputs_size" : obj._inputs_size,
                        "hidden_layers_sizes": obj._hidden_layers_sizes,
                       "outputs_size" : obj._outputs_size
                      }}

          ## Encodel ayers
          layer_encoder = LayerJsonEncoder()
          net_dct["Network"]["layers"] = [layer_encoder.default(layer) for layer in obj._layers]

          return net_dct

      return json.JSONEncoder.default(self, obj)

class NetworkJsonDecoder(json.JSONDecoder):
    # "Hook" de nuestro decoder al decoder de json
    def __init__(self, *args, **kwargs):
      json.JSONDecoder.__init__(self, object_hook=self.object_hook, *args, **kwargs)

    def object_hook (self, dct):
      if "Network" in dct:
        inputs_size = dct["Network"]["inputs_size"]
        outputs_size = dct["Network"]["outputs_size"]
        hidden_layers_sizes = dct["Network"]["hidden_layers_sizes"]

         ## Encodel ayers
        layer_decoder = LayerJsonDecoder()
        layers = []
        for layer_dct in dct["Network"]["layers"]:
          layer = layer_decoder.object_hook(layer_dct)
          layers.append(layer)
          
        network = Network(inputs_size=inputs_size, hidden_layers_sizes=hidden_layers_sizes, outputs_size=outputs_size)
        network._layers = layers
        return network

      return dct


class Network(object):
  # Constructor
  def __init__(self, inputs_size, hidden_layers_sizes=[3], outputs_size=3, learning_rate=0.1):
    self._inputs_size = inputs_size
    self._hidden_layers_sizes = hidden_layers_sizes
    self._outputs_size = outputs_size

    sizes = [inputs_size] + hidden_layers_sizes + [outputs_size]

    # Conecta los layers
    # El input de cada layer es el output del anterior
    layers = []
    for inputs_X_size, outputs_Y_size in zip(sizes, sizes[1:]):
      layer = Layer(inputs_size=inputs_X_size, 
                    outputs_size=outputs_Y_size,
                    learning_rate=learning_rate)
      layers.append(layer)

    self._layers = layers


  def __repr__(self):
      return json.dumps(self, cls=NetworkJsonEncoder)

  def inputs_size(self):
      return self._inputs_size

  def outputs_size(self):
      return self._outputs_size

  def __len__(self):
    return 1 + len(self._hidden_layers_sizes)
    
  def set_activation_function(self, activation_function_id, layer_index=None):
    layers = []
    if layer_index == None:
      layers = self._layers
    else:
      layers.append(self._layers[layer_index])

    for layer in layers:
      layer.set_activation_function(activation_function_id)


  def predict(self, inputs_X):
    layers_net_sum_H, layers_activations_Y = self.feedforward(inputs_X)
    outputs_Y = layers_activations_Y[-1]
    return outputs_Y

  def feedforward(self, inputs_X):
    layers_net_sum_H = []
    layers_activations_Y  = []

    for layer in self._layers:
      net_sum_H, activations_Y = layer.feedforward(inputs_X)
      layers_net_sum_H.append(net_sum_H)
      layers_activations_Y.append(activations_Y)
      inputs_X = activations_Y

    outputs_Y = activations_Y
    ## El total de activaciones debe ser igual al "largo" en profundidad de la red
    assert (len(layers_activations_Y) == len(self))
    return layers_net_sum_H, layers_activations_Y



  def train( self, inputs_X, targets_Z):
    ## 1º) Etapa de feedforward
    layers_net_sum_H, layers_activations_Y = self.feedforward(inputs_X)
    ## 2º) Etapa de backpropagation
    loss = self.backpropagation(inputs_X, targets_Z, layers_net_sum_H, layers_activations_Y)
    return loss

  def mean_squared_error(self, outputs_Y, targets_Z):
    # MSE per row
    # num.mean( num.sum( num.square( Zh-Y2), axis=1))
    #return ((targets_Z - outputs_Y) ** 2).mean(axis=1)
    return np.mean(np.sum(np.square(targets_Z -outputs_Y), axis=1))

  def backpropagation(self, inputs_X, targets_Z, layers_net_sum_H, layers_activations_Y):
    ## 2º) Calculamos el error / loss y su gradiente para el backparopagation
    outputs_Y = layers_activations_Y[-1]
    loss = self.mean_squared_error(outputs_Y, targets_Z)

    ## El grad_outputs_Y es lo que se envia a cada capa para resolver el backpropagation
    grad_outputs_Y = (targets_Z - outputs_Y)

    ## Construimos los inputs de cada layer con las activaciones
    ## El grad_outputs_X de una capa es el grad_outputs_Y de la capa anterior
    layers_inputs_X = [inputs_X] + layers_activations_Y[:-1]
    for net_sums_H, inputs_X, layer in zip(reversed(layers_net_sum_H), reversed(layers_inputs_X), reversed(self._layers)):
      grad_outputs_X = layer.backpropagation(inputs_X, net_sums_H, grad_outputs_Y)
      grad_outputs_Y = grad_outputs_X

    return loss

	
class AccuracyCalculator(object):
    def calculate(self):
      raise NotImplementedError("AccuracyCalculator implementation should implement accuracy.")

class ErrorToleranceAccuracyCalculator(AccuracyCalculator):
  def __init__(self, normalizer, error_tolerance = 0.1):
    self._normalizer = normalizer
    self._error_tolerance = error_tolerance 

  def calculate(self, predicted_Y, expected_Y):
    difference = (expected_Y - predicted_Y) 
    #return np.mean( np.sum( np.square( difference), axis=1))
    return np.mean( np.sqrt(np.sum(np.square( difference),axis=1))<= self._error_tolerance)


#
# Consideramos una prediccion valida si dista proporcionalmente en un valor razonable del esperado.
# Utiliza los valores desnormalizados.
# Por ejemplo con un proportional_error_tolerance de 0.1 si el valor esperado de un output es 1400
# y el valor obtenido es 1400-1388 = 12 < 140 = 1400 * 0.1, este valor se considera aceptable
#
class ProportionalErrorToleranceAccuracyCalculator(AccuracyCalculator):
  def __init__(self, normalizer, proportional_error_tolerance = 0.1):
    self._normalizer = normalizer
    self._proportional_error_tolerance = proportional_error_tolerance 

  def calculate(self, predicted_Y, expected_Y):
    denormalized_expected_Y = normalizer.denormalize(expected_Y)
    denormalized_predicted_Y = normalizer.denormalize(predicted_Y)
    difference = (denormalized_expected_Y - denormalized_predicted_Y)
    error = np.sqrt(np.sum(np.square( difference),axis=1))

    #return np.mean( np.sum( np.square( difference), axis=1))
    return np.mean( error <= expected_Y * self._proportional_error_tolerance)
  



class NNModelJsonEncoder(json.JSONEncoder):
    def default (self, obj):
      if  isinstance(obj, NNModel):
        model_dct = {"NNModel":
                     {"batch_size" : obj._batch_size,
                      "error_tolerance": obj._error_tolerance,
                      "epochs" : obj._epochs
                      }}
                        
        input_encoder = None
        if isinstance(obj._input_normalizer, MinMaxNormalizer) :
          input_encoder = MinMaxNormalizerJsonEncoder()
        elif isinstance(obj._input_normalizer, StandarizationNormalizer):
          input_encoder = StandarizationNormalizerJsonEncoder()

        outout_encoder = None
        if isinstance(obj._output_normalizer, MinMaxNormalizer) :
          outout_encoder = MinMaxNormalizerJsonEncoder()
        elif isinstance(obj._output_normalizer, StandarizationNormalizer):
          outout_encoder = StandarizationNormalizerJsonEncoder()
  
        model_dct["NNModel"]["input_normalizer"] = input_encoder.default(obj._input_normalizer)
        model_dct["NNModel"]["output_normalizer"] = outout_encoder.default(obj._output_normalizer)
        ## Encodel ayers
        network_encoder = NetworkJsonEncoder()
        model_dct["NNModel"]["neural_network"] = network_encoder.default( obj._neural_network) 

        return model_dct

      return json.JSONEncoder.default(self, obj)

class NNModelJsonDecoder(json.JSONDecoder):
    # "Hook" de nuestro decoder al decoder de json
    def __init__(self, *args, **kwargs):
      json.JSONDecoder.__init__(self, object_hook=self.object_hook, *args, **kwargs)

    def object_hook (self, dct):
      if "NNModel" in dct:
        print(repr(dct))
        batch_size = dct["NNModel"]["batch_size"]
        error_tolerance = dct["NNModel"]["error_tolerance"]
        epochs = dct["NNModel"]["epochs"]

        normalizer_dct = dct["NNModel"]["input_normalizer"]["Normalizer"]
        input_normalizer_decoder = None
        if normalizer_dct["type"] == "minmax":
          input_normalizer_decoder = MinMaxNormalizerJsonDecoder()
        elif normalizer_dct["type"] == "standarization":
          input_normalizer_decoder = StandarizationNormalizerJsonDecoder()

        normalizer_dct = dct["NNModel"]["output_normalizer"]["Normalizer"]
        output_normalizer_decoder = None
        if normalizer_dct["type"] == "minmax":
          output_normalizer_decoder = MinMaxNormalizerJsonDecoder()
        elif normalizer_dct["type"] == "standarization":
          output_normalizer_decoder = StandarizationNormalizerJsonDecoder()


        input_normalizer = input_normalizer_decoder.object_hook(dct["NNModel"]["input_normalizer"])
        output_normalizer = output_normalizer_decoder.object_hook(dct["NNModel"]["output_normalizer"])


         ## Encodel ayers
        network_decoder = NetworkJsonDecoder()
        network = network_decoder.object_hook(dct["NNModel"]["neural_network"])
        nnmodel = NNModel(neural_network = network,input_normalizer=input_normalizer, output_normalizer=output_normalizer,batch_size=batch_size, error_tolerance=error_tolerance)
        nnmodel._epochs = epochs
        return nnmodel

      return dct



class NNModel(object):
  # Constructor
  def __init__(self, neural_network, input_normalizer, output_normalizer, batch_size=1, error_tolerance=0.1):
    #print(repr(neural_network.inputs_size() ))
    #print(repr(inputs_X[0]))
    self._neural_network = neural_network
    self._epochs = 0
    self._batch_size = batch_size
    self._error_tolerance = error_tolerance
    self._input_normalizer = input_normalizer
    self._output_normalizer = output_normalizer

  def __repr__(self):
    return json.dumps(self, cls=NNModelJsonEncoder) 

  def iterate_minibatches(self, inputs, targets, batchsize, shuffle=False):
    assert len(inputs) == len(targets)
    if shuffle:
      indices = np.random.permutation(len(inputs))
    #for start_idx in trange(0, len(inputs) - batchsize + 1, batchsize):
    for start_idx in range(0, len(inputs) - batchsize+1, batchsize):
      if shuffle:
        excerpt = indices[start_idx:start_idx + batchsize]
      else:
        excerpt = slice(start_idx, start_idx + batchsize)
      yield inputs[excerpt], targets[excerpt]


  def accuracy(self, inputs_X, expected_Y):
    predicted_Y = self.predict(inputs_X)
    difference = (expected_Y - predicted_Y) 
    #return np.mean( np.sum( np.square( difference), axis=1))
    return np.mean( np.sqrt(np.sum(np.square( difference),axis=1))<= self._error_tolerance)

  def predict(self, inputs):
    neural_network = self._neural_network
    return neural_network.predict(inputs)

  def process_epoch(self, inputs_X, targets_Z):    
    neural_network = self._neural_network
    batch_size = self._batch_size
    assert(neural_network.inputs_size() == len(inputs_X[0]))
    assert(neural_network.outputs_size() == len(targets_Z[0]))
    assert(len(inputs_X) == len(targets_Z))

    loss_per_batch = []
    for batch in self.iterate_minibatches(inputs_X, targets_Z, batch_size, shuffle=True):
      #print("Batch: ", repr(batch))
      loss = neural_network.train(batch[0], batch[1])
      #print("Loss: ", repr(loss))
      loss_per_batch.append(loss)

    self._epochs += 1
    
    epoch_accuracy =  self.accuracy(inputs_X, targets_Z)
    return np.mean(loss_per_batch), epoch_accuracy, loss_per_batch

	
	
import json

class Normalizer(object):
  #def __init__(self, values_V):
    
  def normalization(self, values_V):
     raise NotImplementedError("Normalizer implementation should implement normalization.")

  def denormalization(self, normalized_values_V):
      raise NotImplementedError("Normalizer implementation should implement denormalization.")

  def __repr__(self):
     raise NotImplementedError("Normalizer implementation should implement __repr__.")

class StandarizationNormalizerJsonEncoder(json.JSONEncoder):
    def default (self, obj):
      if  isinstance(obj, Normalizer):
          return {"Normalizer": 
                  {
                  "type": "standarization",
                  "mean_mu": obj._mean_mu.tolist(), 
                  "std_sigma": obj._std_sigma.tolist(),
                  }
                }
      return json.JSONEncoder.default(self, obj)

class StandarizationNormalizerJsonDecoder(json.JSONDecoder):
    # "Hook" de nuestro decoder al decoder de json
    def __init__(self, *args, **kwargs):
      json.JSONDecoder.__init__(self, object_hook=self.object_hook, *args, **kwargs)

    def object_hook (self, dct):
      if "Normalizer" in dct and dct["Normalizer"]["type"] == "standarization":
        print(repr(dct))
        mean_mu = dct["Normalizer"]["mean_mu"]
        std_sigma = dct["Normalizer"]["std_sigma"]
        
        normalizer = StandarizationNormalizer(np.array([[0]]))

        normalizer._mean_mu = np.array(mean_mu)
        normalizer._std_sigma = np.array(std_sigma)

        return normalizer

      return dct


class StandarizationNormalizer(Normalizer):
  def __init__(self, values_V):
    self._mean_mu = np.mean(values_V, axis=0)
    self._std_sigma = np.std(values_V, axis=0)

  def normalization(self, values_V):
    mu = self._mean_mu * np.ones_like(values_V)
    return (values_V - mu) / self._std_sigma

  def denormalization(self, normalized_values_V):
      return (normalized_values_V * self._std_sigma) + self._mean_mu

  def __repr__(self):
    return json.dumps(self, cls=StandarizationNormalizerJsonEncoder)

class MinMaxNormalizerJsonEncoder(json.JSONEncoder):
    def default (self, obj):
      if  isinstance(obj, Normalizer):
          return {"Normalizer": 
                  {
                  "type": "minmax",
                  "max": obj._max.tolist(), 
                  "min": obj._min.tolist(),
                  }
                }
      return json.JSONEncoder.default(self, obj)

class MinMaxNormalizerJsonDecoder(json.JSONDecoder):
    # "Hook" de nuestro decoder al decoder de json
    def __init__(self, *args, **kwargs):
      json.JSONDecoder.__init__(self, object_hook=self.object_hook, *args, **kwargs)

    def object_hook (self, dct):
      if "Normalizer" in dct and dct["Normalizer"]["type"] == "minmax":
        print(repr(dct))
        max = dct["Normalizer"]["max"]
        min = dct["Normalizer"]["min"]
        
        normalizer = MinMaxNormalizer(np.array([[0]]))

        normalizer._max = np.array(max) 
        normalizer._min = np.array(min)

        return normalizer

      return dct

class MinMaxNormalizer(Normalizer):
  def __init__(self, values_V):
    self._max = np.max(values_V, axis=0)
    self._min = np.min(values_V, axis=0)

  def normalization(self, values_V):
    diff = (self._max - self._min)
    return (values_V - self._min) / diff

  def denormalization(self, normalized_values_V):
    diff = (self._max - self._min)
    return (normalized_values_V * diff) + self._min

  def __repr__(self):
    return json.dumps(self, cls=MinMaxNormalizerJsonEncoder)